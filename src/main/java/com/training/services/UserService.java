package com.training.services;

import java.util.List;

import com.training.data.entities.User;
import com.training.data.repositories.UserRepo;

public class UserService {

    private UserRepo repo;

    public UserService() {
        repo = new UserRepo();
    }

    public List<User> findAllUsers() {
        return repo.getUsers();
    }

    public User findUserById(int id) {
        return repo.findUserById(id);
    }

    public void removeUserById(int id) {
        repo.removeUserById(id);
    }

    public void addUser(User user) {
        repo.addUser(user);
    }

    public void updateUser(User user) {
        repo.updateUser(user);
    }

}
