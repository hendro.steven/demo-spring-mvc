package com.training.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.data.entities.Product;
import com.training.data.repositories.ProductRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepo repo;

    public Iterable<Product> findAllProducts() {
        return repo.findAll();
    }

    public Product findProductById(long id) {
        return repo.findById(id).orElse(null);
    }

    public void removeProductById(long id) {
        repo.deleteById(id);
    }

    public Product addProduct(Product product) {
        return repo.save(product);
    }

    public Product updateProduct(Product product) {
        return repo.save(product);
    }
}
