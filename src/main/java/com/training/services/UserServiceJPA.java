package com.training.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.data.entities.User;
import com.training.data.repositories.UserRepoJPA;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserServiceJPA {

    @Autowired
    private UserRepoJPA repo;

    public Iterable<User> findAllUsers() {
        return repo.findAll();
    }

    public User findUserById(long id) {
        return repo.findById(id).orElse(null);
    }

    public void removeUserById(long id) {
        repo.deleteById(id);
    }

    public void addUser(User user) {
        repo.save(user);
    }

    public void updateUser(User user) {
        repo.save(user);
    }
}
