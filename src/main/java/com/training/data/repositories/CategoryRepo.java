package com.training.data.repositories;

import org.springframework.data.repository.CrudRepository;

import com.training.data.entities.Category;

public interface CategoryRepo extends CrudRepository<Category, Long> {

}
