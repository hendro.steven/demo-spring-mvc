package com.training.data.repositories;

import org.springframework.data.repository.CrudRepository;

import com.training.data.entities.Product;

public interface ProductRepo extends CrudRepository<Product, Long> {

}
