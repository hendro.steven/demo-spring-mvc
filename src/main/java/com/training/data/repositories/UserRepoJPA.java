package com.training.data.repositories;

import org.springframework.data.repository.CrudRepository;

import com.training.data.entities.User;

public interface UserRepoJPA extends CrudRepository<User, Long> {

}
