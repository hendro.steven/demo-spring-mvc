package com.training.data.repositories;

import java.util.ArrayList;
import java.util.List;

import com.training.data.entities.User;

public class UserRepo {
    private List<User> users = new ArrayList<>();

    public UserRepo() {
        users.add(new User(1, "John", "Doe", 25, "john.doe@gmail.com", ""));
        users.add(new User(2, "Jane", "Doe", 30, "jane.doe@gmail.com", ""));
        users.add(new User(3, "Tom", "Smith", 35, "tom.smith@gmail.com", ""));
        users.add(new User(4, "Jerry", "Smith", 40, "jerry.smith@gmail.com", ""));
        users.add(new User(5, "Samantha", "Fox", 45, "samantha.fox@yahoo.com", ""));
    }

    public List<User> getUsers() {
        return users;
    }

    public User findUserById(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public void removeUserById(int id) {
        // persons.removeIf(person -> person.getId() == id);
        for (User user : users) {
            if (user.getId() == id) {
                users.remove(user);
                break;
            }
        }
    }

    public void addUser(User user) {
        long nextId = users.size() + 1;
        user.setId(nextId);
        users.add(user);
    }

    public void updateUser(User user) {
        for (User p : users) {
            if (p.getId() == user.getId()) {
                p.setFirstName(user.getFirstName());
                p.setLastName(user.getLastName());
                p.setAge(user.getAge());
                p.setEmail(user.getEmail());
                p.setPassword(user.getPassword());
                break;
            }
        }
    }
}
