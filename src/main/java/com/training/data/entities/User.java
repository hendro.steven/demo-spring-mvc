package com.training.data.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "tbl_users")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "First name is required")
    @Column(nullable = false, length = 100)
    private String firstName;

    @NotEmpty(message = "Last name is required")
    @Column(nullable = false, length = 100)
    private String lastName;

    @Column(nullable = false)
    private int age;

    @NotEmpty(message = "Email is required")
    @Email(message = "Invalid email address")
    @Column(unique = true, length = 255, nullable = false)
    private String email;

    @NotEmpty(message = "Password is required")
    @Column(length = 255, nullable = false)
    private String password;

}
